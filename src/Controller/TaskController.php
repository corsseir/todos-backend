<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\Task;
use App\Form\TaskType;

/**
 * Movie controller.
 * @Route("/api", name="api_")
 */
class TaskController extends FOSRestController
{
    /**
     * Lists all Tasks.
     * @Rest\Get("/tasks")
     *
     * @return Response
     */
    public function getTasks()
    {
        $repository = $this->getDoctrine()->getRepository(Task::class);
        $tasks = $repository->findAll();
        foreach ($tasks as $task) {
            error_log($task->getIsDone());
        }
        return $this->handleView($this->view($tasks));
    }
    /**
     * Create Task.
     * @Rest\Post("/tasks")
     *
     * @return Response
     */
    public function createTask(Request $request)
    {
        $task = new Task();
        $form = $this->createForm(TaskType::class, $task);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            $em->flush();
            return $this->handleView($this->view(['status' => 'ok', 'id' => $task->getId()], Response::HTTP_CREATED));
        }
        return $this->handleView($this->view($form->getErrors()));
    }
    /**
     * Update Task.
     * @Rest\Put("/tasks/{taskId}")
     *
     * @return Response
     */
    public function updateTask(int $taskId, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Task::class);
        $task = $repository->find($taskId);
        $form = $this->createForm(TaskType::class, $task);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            // $em->persist($task);
            $em->flush();
            return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
        }
        return $this->handleView($this->view($form->getErrors()));
    }

    /**
     * Delete Task.
     * @Rest\Delete("/tasks/{taskId}")
     *
     * @return Response
     */
    public function deleteTask(int $taskId)
    {
        $repository = $this->getDoctrine()->getRepository(Task::class);
        $task = $repository->find($taskId);
        if ($task) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($task);
            $em->flush();
            return $this->handleView(($this->view(['status' => 'ok'], Response::HTTP_OK)));
        }
        return $this->handleView(($this->view(['status' => 'notfound'], Response::HTTP_NOT_FOUND)));
    }
}
